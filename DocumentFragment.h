/*
 * DocumentFragment.h: interface for the DocumentFragment module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_DOCUMENT_FRAGMENT_H__
#define __DOM_DOCUMENT_FRAGMENT_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM DocumentFragment is represented internally as an xmlNode
 * except that it's not part itself of the Document, i.e. it's not
 * chained in the tree. But it's childs are, it's like a second parent.
 */

typedef domNodeStruct domDocumentFragmentStruct;
typedef domNode domDocumentFragment;

#endif /* __DOM_DOCUMENT_FRAGMENT_H__ */
