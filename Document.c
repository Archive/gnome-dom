/*
 * Document.h: implementation for the Document module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include <stdio.h>
#include "Document.h"
#include "DOMString.h"

#define UNIMPLEMENTED(msg) 				\
    fprintf(stderr, "%s(%d) %s unimplemented\n", __FILE__, __LINE__, msg); \
    *exc = 1; return(NULL); 

/*
 * Macros for args checking.
 */

#define CHECK_DOC(doc)				\
    if (doc == NULL) { *exc = 1; return(NULL); }
#define CHECK_DOMSTRING(str)				\
    if (str == NULL) { *exc = 1; return(NULL); }

/*
 * Document own attriubutes and methods accesses.
 */


