/*
 * orbit-glue.h: interface for the glue code to link with the ORBit code.
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_ORBIT_GLUE_H__
#define __DOM_ORBIT_GLUE_H__

extern int DOM_server(int *argc, char **argv);
#endif /* __DOM_ORBIT_GLUE_H__ */

