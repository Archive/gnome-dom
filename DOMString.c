/*
 * DOMString.c: provides the string conversion routines needed to
 *              talk through Corba using the following IDL definition
 *              for DOMString "typedef sequence<unsigned short> DOMString;"
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include "DOMString.h"

domString domStringNew(void) {
    return(DOMString__alloc());
}

/*
 * Generation of an empty DOMString, the Corba equivalent of a NULL
 * String pointer.
 */
domString domNullString(void) {
    return(DOMString__alloc());
}

/*
 * Duplicate a DOMString.
 */
domString domStringDup(const domString str) {
    domString ret;

    if (str == NULL) return(NULL);

    ret = DOMString__alloc();
    if (ret == NULL) return(NULL);
    
    if (str->_length > 0) {
        ret->_buffer = 
	    CORBA_sequence_CORBA_unsigned_short_allocbuf(str->_length);
	if (ret->_buffer == NULL) {
	    fprintf(stderr, "domStringDup: out of memory\n");
	    return(ret);
	}
	ret->_maximum = ret->_length = str->_length;
        memcpy(ret->_buffer, str->_buffer,
	       str->_length * sizeof(ret->_buffer[0]));
    }
    return(ret);
}

/*
 * Create a DOMString from a C 8 bit string.
 */
domString domStringDup8(const char *str) {
    domString ret;
    int len, i;

    if (str == NULL) return(NULL);

    ret = DOMString__alloc();
    if (ret == NULL) return(NULL);
    
    len = strlen(str);
    len++;
    if (len > 0) {
        ret->_buffer = CORBA_sequence_CORBA_unsigned_short_allocbuf(len);
	if (ret->_buffer == NULL) {
	    fprintf(stderr, "domStringDup8: out of memory\n");
	    return(ret);
	}
	ret->_maximum = ret->_length = len;
        for (i = 0;i < len ;i++) {
	    ret->_buffer[i] = (CORBA_unsigned_short) str[i];
	}
    }
    return(ret);
}

/*
 * Create a DOMString from a 16 bit UTF16 string.
 */
domString domStringDup16(const unsigned short *str) {
    domString ret;
    int len, i;

    if (str == NULL) return(NULL);

    ret = DOMString__alloc();
    if (ret == NULL) return(NULL);
    
    for (len = 0;str[len] != 0;len++);
    len++;

    if (len > 0) {
        ret->_buffer = CORBA_sequence_CORBA_unsigned_short_allocbuf(len);
	if (ret->_buffer == NULL) {
	    fprintf(stderr, "domStringDup16: out of memory\n");
	    return(ret);
	}
	ret->_maximum = ret->_length = len;
        for (i = 0;i < len ;i++) {
	    ret->_buffer[i] = (CORBA_unsigned_short) str[i];
	}
    }
    return(ret);
}

/*
 * Create a C 8 bit string from a DOMString.
 */
char *domToString8(const domString str) {
    char *ret;
    int i;

    if (str == NULL) return(NULL);

    ret = malloc((str->_length) * sizeof(char));
    if (ret == NULL) {
	fprintf(stderr, "domToString8: out of memory\n");
	return(NULL);
    }
    for (i = 0;i < str->_length;i++)
        ret[i] = (char) str->_buffer[i];
    return(ret);
}

/*
 * Create a C 16 bit string from a DOMString.
 */
unsigned short *domToString16(const domString str) {
    unsigned short *ret;
    int i;

    if (str == NULL) return(NULL);

    ret = malloc((str->_length) * sizeof(unsigned short));
    if (ret == NULL) {
	fprintf(stderr, "domToString16: out of memory\n");
	return(NULL);
    }
    for (i = 0;i < str->_length;i++)
        ret[i] = (unsigned short) str->_buffer[i];
    return(ret);
}

/*
 * compare two DOMString.
 */
int domStringCmp(const domString str1, const domString str2) {
    int i;
    int ret = 0;

    if ((str1 == NULL) && (str2 == NULL)) return(0);
    if (str1 == NULL) return(-1);
    if (str2 == NULL) return(1);

    if (str1->_length < str2->_length) return(-1);
    if (str1->_length > str2->_length) return(+1);
    
    for (i = 0;i < str1->_length;i++) {
        ret = str1->_buffer[i] - str2->_buffer[i];
	if (ret != 0) return(ret);
    }

        
    return(ret);
}

/*
 * compare a DOMString to a C 8 bit string.
 */
int domStringCmp8(const domString str1, const char *str2) {
    int i;
    int len1;
    int len2;
    int ret = 0;

    if ((str1 == NULL) && (str2 == NULL)) return(0);
    if (str1 == NULL) return(-1);
    if (str2 == NULL) return(1);

    len1 = str1->_length - 1;
    len2 = strlen(str2);
    if (len1 < len2) return(-1);
    if (len1 > len2) return(+1);
    
    for (i = 0;i < str1->_length;i++) {
        ret = str1->_buffer[i] - ((CORBA_unsigned_short) str2[i]);
	if (ret != 0) return(ret);
    }
        
    return(ret);
}

/*
 * compare a DOMString to a 16 bit UTF16 string.
 */
int domStringCmp16(const domString str1, const unsigned short *str2) {
    int i;
    int len1;
    int len2;
    int ret = 0;

    if ((str1 == NULL) && (str2 == NULL)) return(0);
    if (str1 == NULL) return(-1);
    if (str2 == NULL) return(1);

    len1 = str1->_length - 1;
    for (len2 = 0;str2[len2] != 0;len2++);

    if (len1 < len2) return(-1);
    if (len1 > len2) return(+1);
    
    for (i = 0;i < len1;i++) {
        ret = str1->_buffer[i] - ((CORBA_unsigned_short) str2[i]);
	if (ret != 0) return(ret);
    }
        
    return(ret);
}

