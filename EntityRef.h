/*
 * EntityRef.h: interface for the EntityReference module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_ENTITY_REF_H__
#define __DOM_ENTITY_REF_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM EntityReference is represented internally as an xmlNode.
 */

typedef domNodeStruct domEntityReferenceStruct;
typedef domNode domEntityReference;

#endif /* __DOM_ENTITY_REF_H__ */
