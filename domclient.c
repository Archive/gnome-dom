/*
 * domclient.c: client test program for the domserver.
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */
#include <stdio.h>
#include <orb/orbit.h>
#include "DOM.h"
#include "DOMString.h"

DOM_DOMImplementation DOMImplementation_client, bec;
DOM_Parser DOMParser_client;

int
main (int argc, char *argv[])
{
    CORBA_Environment ev;
    CORBA_ORB orb;
    CORBA_long rv;
    CORBA_boolean res;
    DOM_Document doc;
    DOMString *name;
    int i;

    /*
     * ORBit stuff
     */
    CORBA_exception_init(&ev);
    orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

    if(argc < 4)
      {
	printf("Need a binding ID thing as argv[1] and argv[2]\n");
	printf("and an XML file name as argv[3]\n");
	return 1;
      }

    /*
     * Create the scions.
     */
    DOMImplementation_client = CORBA_ORB_string_to_object(orb, argv[1], &ev);
    if (!DOMImplementation_client) {
	printf("Cannot bind to %s\n", argv[1]);
	return 1;
    }
    DOMParser_client = CORBA_ORB_string_to_object(orb, argv[2], &ev);
    if (!DOMParser_client) {
	printf("Cannot bind to %s\n", argv[2]);
	return 1;
    }

    res = DOM_DOMImplementation_hasFeature(DOMImplementation_client,
                    domStringDup8("XML"), domStringDup8("1.0"), &ev);
    if(ev._major != CORBA_NO_EXCEPTION) {
      printf("we got exception %d from hasFeature!\n", ev._major);
      return 1;
    }

    if (res == 0)
	printf("hasFeature = false *** PBM ***\n");
    else if (res == 1)
	printf("hasFeature = true\n");
    else 
	printf("hasFeature = %d *** PBM ***\n");

    doc = DOM_Parser_xmlParseFile(DOMParser_client,
				  domStringDup8(argv[3]), &ev);
    if (ev._major != CORBA_NO_EXCEPTION) {
	printf("we got exception %d from xmlParseFile!\n", ev._major);
	return(1);
    }
    if (doc == NULL) {
	printf("No document read !\n");
    } else {
	printf("Got document %p\n", doc);
    }

    name = DOM_Document__get_nodeName(doc, &ev);
    printf("Name returned: %s\n", domToString8(name));

    CORBA_Object_release(DOMImplementation_client, &ev);
    if(ev._major != CORBA_NO_EXCEPTION) {
      printf("we got exception %d from release!\n", ev._major);
      return 1;
    }
    CORBA_Object_release(DOMParser_client, &ev);
    if(ev._major != CORBA_NO_EXCEPTION) {
      printf("we got exception %d from release!\n", ev._major);
      return 1;
    }
    CORBA_Object_release((CORBA_Object)orb, &ev);

    return 0;
}
