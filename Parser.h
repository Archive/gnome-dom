/*
 * Parser.h: interface for the Parser module for DOM.
 *
 * Specification: none, this is NOT covered by DOM Level 1 and subject
 *                to changes.
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_XML_PARSER_H__
#define __DOM_XML_PARSER_H__
#include <tree.h>
#include "DOM.h"
#include "Document.h"

extern domDocument domParseFile (domString filename, int *exc);
extern domDocument domParseDoc (domString content, int *exc);
#endif /* __DOM_XML_PARSER_H__ */
