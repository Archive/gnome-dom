/*
 * tstDOM.c : A DOM test runtime. Takes load and parse XML docs, and then
 *            provides the DOM interface to walk and modify them.
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include "DOM.h"

int main(int argc, char **argv) {
    /* Empty right now ... Need to stbilize the C interfaces */
    exit(0);
}

