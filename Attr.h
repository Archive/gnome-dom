/*
 * Attr.h: interface for the Attr module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_ATTR_H__
#define __DOM_ATTR_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM Attr is represented internally as an specific xmlAttr.
 */

typedef domNodeStruct domAttrStruct;
typedef domNode domAttr;

extern domString domGetName(domAttr attr, int *exc);
extern int domGetSpecified(domAttr attr, int *exc);
extern domString domGetValue(domAttr attr, int *exc);
extern void domSetValue(domAttr attr, domString value, int *exc);

#endif /* __DOM_ATTR_H__ */
