/*
 * Implementation.h: interface for the Implementation module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_IMPLEMENTATION_H__
#define __DOM_IMPLEMENTATION_H__
#include "DOM.h"
#include "DOMString.h"

typedef struct domDOMImplementation {
    int level;		/* 1 */
    int sublevel;	/* 0 */
} *domDOMImplementation;

extern struct domDOMImplementation domCurrentImplementation;

int domImplementationHasFeature(domString feature, domString version, int *exc);
#endif /* __DOM_IMPLEMENTATION_H__ */
