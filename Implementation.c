/*
 * Implementation.h: interface for the Implementation module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include "Implementation.h"
#include "DOMString.h"

struct domDOMImplementation domCurrentImplementation = {
    1, 0  /* XML REQ 1.0 */
};

int domImplementationHasFeature(domString feature, domString version,
                                int *exc) {
    if (!domStringCmp8(feature, "XML")) {
	if (!domStringCmp8(version, "1.0"))
            return(1);
    }
    return(0);
}
