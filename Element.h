/*
 * Element.h: interface for the Element module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_ELEMENT_H__
#define __DOM_ELEMENT_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"
#include "Attr.h"
#include "NodeList.h"

/*
 * A DOM Element is represented internally as an xmlNode.
 */

typedef domNodeStruct domElementStruct;
typedef domNode domElement;

extern domString domGetTagName(domElement elem, int *exc);
extern domString domGetAttribute(domElement elem, domString name, int *exc);
extern void domSetAttribute(domElement elem, domString name, 
                            domString value, int *exc);
extern void domRemoveAttribute(domElement elem, domString name, int *exc);
extern domAttr domGetAttributeNode(domElement elem, domString name, int *exc);
extern domAttr domSetAttributeNode(domElement elem, domAttr newAttr, int *exc);
extern void domRemoveAttributeNode(domElement elem, domAttr oldAttr, int *exc);
extern domNodeList domGetElementsByTagName(domNode elem, domString name,
                                           int *exc);
extern void domNormalize(domElement elem, int *exc);

#endif /* __DOM_ELEMENT_H__ */
