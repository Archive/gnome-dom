/*
 * DOMString.c: interface to the string conversion routines needed to
 *              talk through Corba using the following IDL definition
 *              for DOMString "typedef sequence<unsigned short> DOMString;"
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_DOMSTRING_H__
#define __DOM_DOMSTRING_H__
#include "DOM.h"

typedef DOMString *DOMStringPtr;
typedef DOMStringPtr domString;

extern domString domNullString(void);
extern domString domStringDup(const domString str);
extern domString domStringDup8(const char *str);
extern domString domStringDup16(const unsigned short *str);
extern char *domToString8(const domString str);
extern unsigned short *domToString16(const domString str);
extern int domStringCmp(const domString str1, const domString str2);
extern int domStringCmp8(const domString str1, const char *str2);
extern int domStringCmp16(const domString str1, const unsigned short *str2);

#endif /* __DOM_DOMSTRING_H__ */
