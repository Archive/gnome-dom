/*
 * NodeList.h: interface for the NodeList module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_NODE_LIST_H__
#define __DOM_NODE_LIST_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM NodeList is represented internally as an specific xmlNodeList.
 */

typedef struct domNodeListStruct {
    int todo;   /* A real pain ! */
} domNodeListStruct, *domNodeList;

extern domNode domNodeListItem(domNodeList list, unsigned long index, int *exc);
extern unsigned long domGetNodeListLength(domNodeList list, int *exc);

#endif /* __DOM_NODE_LIST_H__ */
