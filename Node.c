/*
 * Node.c : implementation of the DOM Node interface.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include <stdio.h>
#include "DOMString.h"
#include "Node.h"  

/* Include file of the XML tree implementation */
#include "tree.h"

#define UNIMPLEMENTED(msg) 				\
    fprintf(stderr, "%s(%d) %s unimplemented\n", __FILE__, __LINE__, msg); \
    *exc = 1; return(NULL); 

/*
 * Macros for args checking.
 */
#define CHECK_NODE(node)				\
    if (node == NULL) { *exc = 1; return(NULL); }
#define CHECK_DOMSTRING(str)				\
    if (str == NULL) { *exc = 1; return(NULL); }


domString domGetNodeName(domNode node, int *exc) {
    CHECK_NODE(node)

    switch (node->node.type) {
	case DOM_ELEMENT_NODE:
	    return(domStringDup8(node->node.name));
	case DOM_ATTRIBUTE_NODE:
	    return(domStringDup8(node->attr.name));
        case DOM_TEXT_NODE:
	    return(domStringDup8("#text"));
        case DOM_CDATA_SECTION_NODE:
	    return(domStringDup8("#cdata-section"));
        case DOM_ENTITY_REFERENCE_NODE:
	    UNIMPLEMENTED("domGetNodeName on entity refs");
	    return(NULL);
        case DOM_ENTITY_NODE:
	    UNIMPLEMENTED("domGetNodeName on entity");
	    return(NULL);
        case DOM_PI_NODE:
	    UNIMPLEMENTED("domGetNodeName on PI");
	    return(NULL);
        case DOM_COMMENT_NODE:
	    return(domStringDup8("#comment"));
        case DOM_DOCUMENT_NODE:
	    return(domStringDup8("#document"));
        case DOM_DOCUMENT_TYPE_NODE:
	    UNIMPLEMENTED("domGetNodeName on document type");
	    return(NULL);
        case DOM_DOCUMENT_FRAGMENT_NODE:
	    return(domStringDup8("#document-fragment"));
        case DOM_NOTATION_NODE:
	    UNIMPLEMENTED("domGetNodeName on notation");
	    return(NULL);
    }
    fprintf(stderr, "domGetNodeName : invalid type %d\n", node->node.type);
    return(NULL);
}

domString domGetNodeValue(domNode node, int *exc) {
    CHECK_NODE(node)

    switch (node->node.type) {
	case DOM_ELEMENT_NODE:
	    return(NULL);
	case DOM_ATTRIBUTE_NODE: {
	    CHAR *val;
	    domString ret;
	    val = xmlNodeGetContent(node->attr.val);
	    ret = domStringDup8(val);
	    if (val) free(val);
	    return(ret);
	}
        case DOM_TEXT_NODE: {
	    CHAR *val;
	    domString ret;
	    val = xmlNodeGetContent(&(node->node));
	    ret = domStringDup8(val);
	    if (val) free(val);
	    return(ret);
	}
	    return(domStringDup8(node->node.content));
        case DOM_CDATA_SECTION_NODE: {
	    CHAR *val;
	    domString ret;
	    val = xmlNodeGetContent(&(node->node));
	    ret = domStringDup8(val);
	    if (val) free(val);
	    return(ret);
	}
        case DOM_ENTITY_REFERENCE_NODE:
	    return(NULL);
        case DOM_ENTITY_NODE:
	    return(NULL);
        case DOM_PI_NODE: {
	    CHAR *val;
	    domString ret;
	    val = xmlNodeGetContent(&(node->node));
	    ret = domStringDup8(val);
	    if (val) free(val);
	    return(ret);
	}
        case DOM_COMMENT_NODE: {
	    CHAR *val;
	    domString ret;
	    val = xmlNodeGetContent(&(node->node));
	    ret = domStringDup8(val);
	    if (val) free(val);
	    return(ret);
	}
        case DOM_DOCUMENT_NODE:
	    return(NULL);
        case DOM_DOCUMENT_TYPE_NODE:
	    return(NULL);
        case DOM_DOCUMENT_FRAGMENT_NODE:
	    return(NULL);
        case DOM_NOTATION_NODE:
	    return(NULL);
    }
    fprintf(stderr, "domGetNodeValue : invalid type %d\n", node->node.type);
    return(NULL);
}

int domSetNodeValue(domNode node, domString value, int *exc) {
    char *content;
    if (node == NULL) { *exc = 1; return(-1); }
    if (value == NULL) { *exc = 1; return(-1); }

    switch (node->node.type) {
	case DOM_ELEMENT_NODE:
	    return(0);
	case DOM_ATTRIBUTE_NODE: {
	    xmlNodePtr childs;

	    content = domToString8(value);
	    childs = xmlStringGetNodeList(NULL, content); /* Get doc !!! */
	    xmlFreeNodeList(node->attr.val);
	    node->attr.val = childs;
	    free(content);
	    return(0);
	}
        case DOM_TEXT_NODE:
	    content = domToString8(value);
	    xmlNodeSetContent(&(node->node), content);
	    free(content);
	    return(0);
        case DOM_CDATA_SECTION_NODE:
	    content = domToString8(value);
	    xmlNodeSetContent(&(node->node), content);
	    free(content);
	    return(0);
        case DOM_ENTITY_REFERENCE_NODE:
	    return(0);
        case DOM_ENTITY_NODE:
	    return(0);
        case DOM_PI_NODE:
	    content = domToString8(value);
	    xmlNodeSetContent(&(node->node), content);
	    free(content);
	    return(0);
        case DOM_COMMENT_NODE:
	    content = domToString8(value);
	    xmlNodeSetContent(&(node->node), content);
	    free(content);
	    return(0);
        case DOM_DOCUMENT_NODE:
	    return(0);
        case DOM_DOCUMENT_TYPE_NODE:
	    return(0);
        case DOM_DOCUMENT_FRAGMENT_NODE:
	    return(0);
        case DOM_NOTATION_NODE:
	    return(0);
    }
    fprintf(stderr, "domSetNodeValue : invalid type %d\n", node->node.type);
    return(0);
}

unsigned short domGetNodeType(domNode node, int *exc) {
    if (node == NULL) { *exc = 1; return(-1); }

    /* 
     * NOTE: the mapping is maintained in gnome-xml/tree.h
     */
    return(node->node.type);
}

domNode domGetParentNode(domNode node, int *exc) {
    CHECK_NODE(node)

    /*
     * TODO Handling the case of doc->root !!!
     */
    switch (node->node.type) {
	case DOM_ATTRIBUTE_NODE:
        case DOM_DOCUMENT_NODE:
        case DOM_DOCUMENT_FRAGMENT_NODE:
	    return(NULL);
	case DOM_ELEMENT_NODE:
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_ENTITY_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    return((domNode) node->node.parent);
    }
    fprintf(stderr, "domGetParentNode : invalid type %d\n", node->node.type);
    return(NULL);
}

domNode domGetFirstChild(domNode node, int *exc) {
    CHECK_NODE(node)

    switch (node->node.type) {
	case DOM_ATTRIBUTE_NODE:
	    return(NULL); /* TODO !!! */
        case DOM_DOCUMENT_NODE:
	    return((domNode) node->doc.root);
        case DOM_DOCUMENT_FRAGMENT_NODE:
	case DOM_ELEMENT_NODE:
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_ENTITY_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    return((domNode) node->node.childs);
    }
    fprintf(stderr, "domGetFirstChild : invalid type %d\n", node->node.type);
    return(NULL);
}

domNode domGetLastChild(domNode node, int *exc) {
    CHECK_NODE(node)

    switch (node->node.type) {
	case DOM_ATTRIBUTE_NODE:
	    return(NULL); /* TODO !!! */
        case DOM_DOCUMENT_NODE: {
	    xmlNodePtr cur = node->doc.root;
	    if (cur == NULL) return(NULL);
	    while (cur->next != NULL) cur = cur->next;
	    return((domNode) cur);
	}
        case DOM_DOCUMENT_FRAGMENT_NODE:
	case DOM_ELEMENT_NODE:
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_ENTITY_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    return((domNode) xmlGetLastChild(node->node.childs));
    }
    fprintf(stderr, "domGetLastChild : invalid type %d\n", node->node.type);
    return(NULL);
}

domNode domGetPreviousSibling(domNode node, int *exc) {
    CHECK_NODE(node)

    switch (node->node.type) {
	case DOM_ATTRIBUTE_NODE:
	    return(NULL);
        case DOM_DOCUMENT_NODE:
	    return(NULL);
        case DOM_DOCUMENT_FRAGMENT_NODE:
	case DOM_ELEMENT_NODE:
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_ENTITY_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    return((domNode) node->node.prev);
    }
    fprintf(stderr, "domGetPreviousSibling : invalid type %d\n",
            node->node.type);
    return(NULL);
}

domNode domGetNextSibling(domNode node, int *exc) {
    CHECK_NODE(node)

    switch (node->node.type) {
	case DOM_ATTRIBUTE_NODE:
	    return(NULL);
        case DOM_DOCUMENT_NODE:
	    return(NULL);
        case DOM_DOCUMENT_FRAGMENT_NODE:
	case DOM_ELEMENT_NODE:
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_ENTITY_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    return((domNode) node->node.next);
    }
    fprintf(stderr, "domGetPreviousSibling : invalid type %d\n",
            node->node.type);
    return(NULL);
}

domDocument  domGetOwnerDocument(domNode node, int *exc) {
    CHECK_NODE(node)

    switch (node->node.type) {
	case DOM_ATTRIBUTE_NODE:
	    if (node->attr.node != NULL)
	        return((domDocument) node->attr.node->doc);
	    return(NULL);
        case DOM_DOCUMENT_NODE:
	    return(NULL);
        case DOM_DOCUMENT_FRAGMENT_NODE:
	case DOM_ELEMENT_NODE:
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_ENTITY_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    return((domDocument) node->node.doc);
    }
    fprintf(stderr, "domGetPreviousSibling : invalid type %d\n",
            node->node.type);
    return(NULL);
}

domNode domInsertBefore(domNode cur, domNode nChild, domNode rChild, int *exc) {
    xmlNodePtr anc;
    xmlNodePtr node = (xmlNodePtr) cur;
    xmlNodePtr newChild = (xmlNodePtr) nChild;
    xmlNodePtr refChild = (xmlNodePtr) rChild;
    xmlDocPtr doc = NULL;
    CHECK_NODE(cur)
    CHECK_NODE(newChild)

    switch(node->type) {
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    *exc = DOM_HIERARCHY_REQUEST_ERR;
	    return(NULL);
    }
    switch(newChild->type) {
        case DOM_DOCUMENT_NODE:
        case DOM_ATTRIBUTE_NODE:
	    *exc = DOM_HIERARCHY_REQUEST_ERR;
	    return(NULL);
    }

    /*
     * Deal with the case the parent is the Document.
     */
    if (node->type == DOM_DOCUMENT_NODE) {
	/* TODO !!!
	if ((newChild->node.doc != NULL) &&
	    (newChild->node.doc != (xmlDocPtr) cur)) {
	    *exc = DOM_WRONG_DOCUMENT_ERR;
	    return(NULL);
	}
	 */
	UNIMPLEMENTED("domInsertBefore on document");
	return(NULL);
    }

    /*
     * Deal with the case the parent is an Attribute.
     */
    if (cur->node.type == DOM_ATTRIBUTE_NODE) {
	/* TODO !!! */
	UNIMPLEMENTED("domInsertBefore on attributes");
	return(NULL);
    }

    /*
     * Normal case, the parent is a xmlNodePtr.
     * Check that the new child pertains to the same document.
     */
    if ((node->doc != NULL) && (newChild->doc != NULL) &&
	(node->doc != newChild->doc)) {
	*exc = DOM_WRONG_DOCUMENT_ERR;
	return(NULL);
    }

    /*
     * Check first that refChild parent is node.
     */
    if ((refChild != NULL) && (refChild->parent != node)) {
        *exc = DOM_NOT_FOUND_ERR;
	return(NULL);
    }

    /*
     * Check that refChild is not one of the ancestors of node.
     */
    anc = node;
    while (anc != NULL) {
	if (anc == newChild) {
	    *exc = DOM_HIERARCHY_REQUEST_ERR;
	    return(NULL);
	}
	anc = anc->parent;
    }

    /*
     * check for insertion at the end.
     */
    if (refChild == NULL) {
        xmlNodePtr last = node->childs;

	/*
	 * The current node has no child.
	 */
        if (last == NULL) {
	    /*
	     * Check for DocumentFragment.
	     */
	    if (newChild->type == DOM_DOCUMENT_FRAGMENT_NODE) {
		xmlNodePtr first;

		first = newChild->childs;
		if (first == NULL) return((domNode) newChild);

                node->childs = first;
		while (first->next != NULL) {
		    first->parent = NULL;
		    first = first->next;
		}
	    } else {
		newChild->parent = node;
		newChild->next = NULL;
		newChild->prev = NULL;
	    }
	} else {
	    /*
	     * Check for DocumentFragment.
	     */
	    if (newChild->type == DOM_DOCUMENT_FRAGMENT_NODE) {
		xmlNodePtr first;

		first = newChild->childs;
		if (first == NULL) return((domNode) newChild);

		first->prev = last;
		last->next = first;

		first->parent = node;
		while (first->next != NULL) {
		    first->parent = node;
		    first = first->next;
		}
	    } else {
		newChild->parent = node;
		newChild->next = NULL;
		newChild->prev = last;
		last->next = newChild;
	    }
	}
    } else {
	/*
	 * Check for DocumentFragment.
	 */
	if (newChild->type == DOM_DOCUMENT_FRAGMENT_NODE) {
	    xmlNodePtr first, last;

	    first = last = newChild->childs;
	    if (first == NULL) return((domNode) newChild);

	    while (last->next != NULL) {
		last->parent = node;
		last = last->next;
	    }
	    last->next = refChild;
	    first->prev = refChild->prev;
	    refChild->prev = last;
	    if (first->prev != NULL)
		first->prev->next = first;
	    else
		node->childs = first;
	} else {
	    newChild->parent = node;
	    newChild->next = refChild;
	    newChild->prev = refChild->prev;
	    refChild->prev = newChild;
	    if (newChild->prev != NULL)
		newChild->prev->next = newChild;
	    else
		node->childs = newChild;
	}
    }
    return((domNode) newChild);
}

domNode domReplaceChild(domNode node,
                      domNode newChild, domNode oldChild, int *exc) {
    CHECK_NODE(node)

    UNIMPLEMENTED("domReplaceChild");
}

domNode domRemoveChild(domNode cur,
                      domNode child, int *exc) {
    xmlNodePtr node = (xmlNodePtr) cur;
    xmlNodePtr oldChild = (xmlNodePtr) child;
    CHECK_NODE(node)
    CHECK_NODE(oldChild)

    switch(node->type) {
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    *exc = DOM_HIERARCHY_REQUEST_ERR;
	    return(NULL);
    }
    switch(oldChild->type) {
        case DOM_DOCUMENT_NODE:
        case DOM_ATTRIBUTE_NODE:
	    *exc = DOM_HIERARCHY_REQUEST_ERR;
	    return(NULL);
    }

    /*
     * Normal case, the parent is a xmlNodePtr.
     * Check that the new child pertains to the same document.
     */
    if ((node->doc != NULL) && (oldChild->doc != NULL) &&
	(node->doc != oldChild->doc)) {
	*exc = DOM_WRONG_DOCUMENT_ERR;
	return(NULL);
    }

    /*
     * Check first that refChild parent is node.
     */
    if ((oldChild != NULL) && (oldChild->parent != node)) {
        *exc = DOM_NOT_FOUND_ERR;
	return(NULL);
    }

    /*
     * Check if node is a document
     */
    if (node->type == DOM_DOCUMENT_NODE) {
	xmlDocPtr doc = (xmlDocPtr) cur;
        if (doc->root == oldChild) 
	    doc->root = oldChild->next;
    }
    else {
        if (node->childs == oldChild)
	    node->childs = oldChild->next;
    }
    if (oldChild->prev != NULL)
	oldChild->prev->next = oldChild->next;
    oldChild->next = oldChild->prev = NULL;
    oldChild->parent = NULL;

    return((domNode) oldChild);
}

domNode domAppendChild(domNode node,
                      domNode newChild, int *exc) {
    CHECK_NODE(node)

    UNIMPLEMENTED("domAppendChild");
}

int domHasChildNodes(domNode node, int *exc) {
    if (node == NULL) { *exc = 1; return(-1); }

    return(0);
    switch (node->node.type) {
	case DOM_ATTRIBUTE_NODE:
	    UNIMPLEMENTED("domHasChildNodes on attributes");
	    return(0);
        case DOM_DOCUMENT_NODE:
	    if (node->doc.root != NULL) return(1);
	    return(0);
        case DOM_DOCUMENT_FRAGMENT_NODE:
	case DOM_ELEMENT_NODE:
        case DOM_TEXT_NODE:
        case DOM_CDATA_SECTION_NODE:
        case DOM_ENTITY_REFERENCE_NODE:
        case DOM_ENTITY_NODE:
        case DOM_PI_NODE:
        case DOM_COMMENT_NODE:
        case DOM_DOCUMENT_TYPE_NODE:
        case DOM_NOTATION_NODE:
	    if (node->node.childs != NULL) return(1);
	    return(0);
    }
    fprintf(stderr, "domHasChildNodes : invalid type %d\n", node->node.type);
    return(0);
}

domNode domCloneNode(domNode node, int deep, int *exc) {
    CHECK_NODE(node)

    UNIMPLEMENTED("domCloneNode");
}

