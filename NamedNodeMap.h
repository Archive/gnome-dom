/*
 * NamedNodeMap.h: interface for the NamedNodeMap module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_NAMED_NODE_MAP_H__
#define __DOM_NAMED_NODE_MAP_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM NamedNodeMap is represented internally as an specific xmlNamedNodeMap.
 */

typedef struct domNamedNodeMapStruct {
    int todo;   /* A real pain ! */
} domNamedNodeMapStruct, *domNamedNodeMap;

extern domNode domItem(domNamedNodeMap list, unsigned long index, int *exc);
extern unsigned long domGetLength(domNamedNodeMap list, int *exc);

#endif /* __DOM_NAMED_NODE_MAP_H__ */
