/*
 * DocumentType.h: interface for the DocumentType module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_DOCUMENT_TYPE_H__
#define __DOM_DOCUMENT_TYPE_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM DocumentType is represented internally as a specific Node.
 * Actually most of the informations are carried internally by the
 * xmlDoc of the document.
 */

typedef domNodeStruct domDocumentTypeStruct;
typedef domNode domDocumentType;

#endif /* __DOM_DOCUMENT_TYPE_H__ */
