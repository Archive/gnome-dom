/*
 * orbit-glue.c: glue code for the server-side to link with the ORBit
 *               code.
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include <stdio.h>
#include <config.h>
#include <orb/orbit.h>

#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "orbit-glue.h"

#include "Implementation.h"
#include "Parser.h"
#include "Node.h"
#include "Document.h"

/************************************************************************
 *									*
 * Piggy-backing of internal structures on top of Corba objects		*
 *									*
 ************************************************************************/

/*
 * Node are implemented by xmlNodePtr/xmlDocPtr/xmlAttrPtr objects.
 * Note that a Node structure starts with
 *    void *_private;
 *    void *vepv;
 *    int   type;
 *
 * Which allows:
 *   - to use it for DOM_Node
 *   - to detect by looking at the tupe whether the DOM_Node
 *     is actually a Node, and Attribute or a Document.
 */
typedef domNode DOM_NodeServantPiggy;

static DOM_Node DOM_NodeServantRegister(domNode node, CORBA_Environment *ev);

/*
 * Documents are implemented by xmlDocPtr objects.
 */
typedef domDocument DOM_DocumentServantPiggy;

static DOM_Document DOM_DocumentServantRegister(domDocument doc,
                                                CORBA_Environment *ev);

/************************************************************************
 *									*
 *		Macros for C functions wrappers				*
 *  These isolate constructs specific to ORBit and allow "clean" C code *
 *  The only extra parameter is the int * for the exception emulation.	*
 *									*
 ************************************************************************/

int orbitDOMDebug = 3;

/*
 * Stuff handled directly, just a type cast.
 */
#define NATIVE_void int
#define voidObjectReturn(x, ev)
#define DEFINE_PIGGY_void(obj)
#define ACCESS_PIGGY_void(obj)

/*
 * Stuff handled directly, just a type cast.
 */
typedef unsigned short DOMushort;
#define NATIVE_DOMushort unsigned short
#define DOMushortObjectReturn(x, ev) ((unsigned short) x)
#define DEFINE_PIGGY_DOMushort(obj)
#define ACCESS_PIGGY_DOMushort(obj) obj

/*
 * Stuff handled directly, just a type cast.
 */
#define NATIVE_CORBA_boolean int
#define CORBA_booleanObjectReturn(x, ev) ((CORBA_boolean) x)
#define DEFINE_PIGGY_CORBA_boolean(obj)
#define ACCESS_PIGGY_CORBA_boolean(obj) obj

/*
 * Conversion macros for type DOMString
 * We may change the direct type conversion in the future.
 */
#define NATIVE_DOMStringPtr DOMStringPtr
#define DEFINE_PIGGY_DOMStringPtr(obj)
#define ACCESS_PIGGY_DOMStringPtr(obj) obj
#define DOMStringPtrObjectReturn(x, ev)			\
((x == NULL) ? domNullString() : ((DOMStringPtr) x)) 

/*
 * Conversion macros for type Node.
 */
#define NATIVE_DOM_Node domNode
#define DEFINE_PIGGY_DOM_Node(obj)			\
  domNode piggy_##obj = (domNode) obj;
#define ACCESS_PIGGY_DOM_Node(obj)			\
  piggy_##obj
#define DOM_NodeObjectReturn(x, ev) 			\
((x == NULL) ?						\
    CORBA_OBJECT_NIL :					\
    ((x->node.vepv == NULL) ?				\
        DOM_NodeServantRegister(x, ev) :		\
	((DOM_Node) x)))

/*
 * Conversion macros for type NodeList.
#define NATIVE_DOM_NodeList xmlNodePtr
#define DEFINE_PIGGY_DOM_NodeList(obj)			\
  domNodeList piggy_##obj = (domNodeList) obj;
#define ACCESS_PIGGY_DOM_NodeList(obj)			\
  piggy_##obj
#define DOM_NodeListObjectReturn(x, ev) 		\
((x == NULL) ?						\
    CORBA_OBJECT_NIL :					\
    ((x->vepv == NULL) ?				\
        DOM_NodeListServantRegister(x, ev) :		\
	((DOM_NodeList) x)))
 */

/*
 * Conversion macros for type Document.
 */
#define NATIVE_DOM_Document xmlDocPtr
#define DEFINE_PIGGY_DOM_Document(obj)			\
  domDocument piggy_##obj = (domDocument) obj;
#define ACCESS_PIGGY_DOM_Document(obj)			\
  piggy_##obj
#define DOM_DocumentObjectReturn(x, ev) 		\
((x == NULL) ?						\
    CORBA_OBJECT_NIL :					\
    ((x->vepv == NULL) ?				\
        DOM_DocumentServantRegister(x, ev) :		\
	((DOM_Document) x)))



#define DRCT_WRAPPER0(type, name)				\
type ORBIT_##name(PortableServer_Servant servant,		\
                  CORBA_Environment * ev) {			\
    int exc = 0; NATIVE_##type ret;				\
    if (orbitDOMDebug > 2) fprintf(stderr,"DOM: called\n");	\
    ret = name(&exc);						\
    if (exc) {							\
        if (orbitDOMDebug)					\
	    fprintf(stderr,"DOM: raised %d\n", exc);		\
    }								\
    return type##ObjectReturn(ret, ev);				\
}

#define DRCT_WRAPPER1(type, name, t1)				\
type ORBIT_##name(PortableServer_Servant servant,		\
                      t1 a1, CORBA_Environment * ev) {		\
    int exc = 0; NATIVE_##type ret;				\
    DEFINE_PIGGY_##t1(a1)					\
    if (orbitDOMDebug > 2) fprintf(stderr,"DOM: called\n");	\
    ret = name(ACCESS_PIGGY_##t1(a1), &exc);		\
    if (exc) {							\
        if (orbitDOMDebug)					\
	    fprintf(stderr,"DOM: raised %d\n", exc);		\
    }								\
    return type##ObjectReturn(ret, ev);				\
}

#define DRCT_WRAPPER2(type, name, t1, t2)			\
type ORBIT_##name(PortableServer_Servant servant,		\
                      t1 a1, t2 a2, CORBA_Environment * ev) {	\
    int exc = 0; NATIVE_##type ret;				\
    DEFINE_PIGGY_##t1(a1)					\
    DEFINE_PIGGY_##t2(a2)					\
    if (orbitDOMDebug > 2) fprintf(stderr,"DOM: called\n");	\
    ret = name(ACCESS_PIGGY_##t1(a1),			\
    		     ACCESS_PIGGY_##t2(a2), &exc);		\
    if (exc) {							\
        if (orbitDOMDebug)					\
	    fprintf(stderr,"DOM: raised %d\n", exc);		\
    }								\
    return type##ObjectReturn(ret, ev);				\
}

#define VIRT_WRAPPER0(type, object, name)			\
type ORBIT_##name(PortableServer_Servant servant,		\
                      CORBA_Environment * ev) {			\
    int exc = 0; NATIVE_##type ret;				\
    DEFINE_PIGGY_##object(servant)				\
    if (orbitDOMDebug > 2) fprintf(stderr,"DOM: called\n");	\
    ret = name(ACCESS_PIGGY_##object(servant),		\
                             &exc);				\
    if (exc) {							\
        if (orbitDOMDebug)					\
	    fprintf(stderr,"DOM: raised %d\n", exc);		\
    }								\
    return type##ObjectReturn(ret, ev);				\
}

#define VIRT_WRAPPER1(type, object, name, t1)			\
type ORBIT_##name(PortableServer_Servant servant,		\
                      t1 a1, CORBA_Environment * ev) {		\
    int exc = 0; NATIVE_##type ret;				\
    DEFINE_PIGGY_##object(servant)				\
    DEFINE_PIGGY_##t1(a1)					\
    if (orbitDOMDebug > 2) fprintf(stderr,"DOM: called\n");	\
    ret = name(ACCESS_PIGGY_##object(servant),		\
                     ACCESS_PIGGY_##t1(a1), &exc);		\
    if (exc) {							\
        if (orbitDOMDebug)					\
	    fprintf(stderr,"DOM: raised %d\n", exc);		\
    }								\
    return type##ObjectReturn(ret, ev);				\
}

#define VIRT_WRAPPER2(type, object, name, t1, t2)		\
type ORBIT_##name(PortableServer_Servant servant,		\
                      t1 a1, t2 a2, CORBA_Environment * ev) {	\
    int exc = 0; NATIVE_##type ret;				\
    DEFINE_PIGGY_##object(servant)				\
    DEFINE_PIGGY_##t1(a1)					\
    DEFINE_PIGGY_##t2(a2)					\
    if (orbitDOMDebug > 2) fprintf(stderr,"DOM: called\n");	\
    ret = name(ACCESS_PIGGY_##object(servant),		\
                     ACCESS_PIGGY_##t1(a1),			\
    		     ACCESS_PIGGY_##t2(a2), &exc);		\
    if (exc) {							\
        if (orbitDOMDebug)					\
	    fprintf(stderr,"DOM: raised %d\n", exc);		\
    }								\
    return type##ObjectReturn(ret, ev);				\
}


/************************************************************************
 *									*
 *			 ORBit global variables.			*
 *									*
 ************************************************************************/

static PortableServer_ServantBase__epv base_epv = {
  NULL, /* _private */
  NULL, /* finalize */
  NULL, /* use base default_POA function */
};

CORBA_ORB orb = NULL;
CORBA_Environment ev;
PortableServer_POA poa;

/************************************************************************
 *									*
 * Register the server side function handling the incoming CORBA calls.	*
 *									*
 ************************************************************************/

/*
 * Virtual tables for the DOMImplementation: static one.
 */

DRCT_WRAPPER2(CORBA_boolean, domImplementationHasFeature, \
              DOMStringPtr, DOMStringPtr)

static POA_DOM_DOMImplementation__epv DOMImplementation_epv = {
    NULL, /* _private */
    (gpointer)&ORBIT_domImplementationHasFeature
};

static POA_DOM_DOMImplementation__vepv DOMImplementation_vepv =
         { &base_epv, &DOMImplementation_epv };
static POA_DOM_DOMImplementation DOMImplementation_servant =
         { NULL, &DOMImplementation_vepv };
DOM_DOMImplementation DOMImplementation_client = CORBA_OBJECT_NIL;

/*
 * Virtual tables for the Parser: static one.
 */
DRCT_WRAPPER1(DOM_Document, domParseFile, DOMStringPtr)
DRCT_WRAPPER1(DOM_Document, domParseDoc, DOMStringPtr)

static POA_DOM_Parser__epv DOMParser_epv = {
    NULL, /* _private */
    (gpointer)&ORBIT_domParseFile,
    (gpointer)&ORBIT_domParseDoc
};

static POA_DOM_Parser__vepv DOMParser_vepv = { &base_epv, &DOMParser_epv };
static POA_DOM_Parser DOMParser_servant = { NULL, &DOMParser_vepv };
DOM_Parser DOMParser_client = CORBA_OBJECT_NIL;

/*
 * Virtual tables for the Node(s): dynamic ones.
 */
VIRT_WRAPPER0(DOMStringPtr, DOM_Node, domGetNodeName)
VIRT_WRAPPER0(DOMStringPtr, DOM_Node, domGetNodeValue)
VIRT_WRAPPER1(void, DOM_Node, domSetNodeValue, DOMStringPtr)
VIRT_WRAPPER0(DOMushort, DOM_Node, domGetNodeType)
VIRT_WRAPPER0(DOM_Node, DOM_Node, domGetParentNode)
/* VIRT_WRAPPER0(DOM_Node, DOM_NodeList, domGetChildNodes) */
VIRT_WRAPPER0(DOM_Node, DOM_Node, domGetFirstChild)
VIRT_WRAPPER0(DOM_Node, DOM_Node, domGetLastChild)
VIRT_WRAPPER0(DOM_Node, DOM_Node, domGetPreviousSibling)
VIRT_WRAPPER0(DOM_Node, DOM_Node, domGetNextSibling)
/* VIRT_WRAPPER0(DOM_Node, DOM_NamedNodeMap, domGetAttributes) */
VIRT_WRAPPER0(DOM_Document, DOM_Node, domGetOwnerDocument)
VIRT_WRAPPER2(DOM_Node, DOM_Node, domInsertBefore, DOM_Node, DOM_Node)
VIRT_WRAPPER2(DOM_Node, DOM_Node, domReplaceChild, DOM_Node, DOM_Node)
VIRT_WRAPPER1(DOM_Node, DOM_Node, domRemoveChild, DOM_Node)
VIRT_WRAPPER1(DOM_Node, DOM_Node, domAppendChild, DOM_Node)
VIRT_WRAPPER0(CORBA_boolean, DOM_Node, domHasChildNodes)
VIRT_WRAPPER1(DOM_Node, DOM_Node, domCloneNode, CORBA_boolean)

static POA_DOM_Node__epv Node_epv = {
    NULL, /* _private */
    (gpointer)&ORBIT_domGetNodeName,
    (gpointer)&ORBIT_domGetNodeValue,
    (gpointer)&ORBIT_domSetNodeValue,
    (gpointer)&ORBIT_domGetNodeType,
    (gpointer)&ORBIT_domGetParentNode,
    NULL, /* (gpointer)&ORBIT_domGetChildNodes, */
    (gpointer)&ORBIT_domGetFirstChild,
    (gpointer)&ORBIT_domGetLastChild,
    (gpointer)&ORBIT_domGetPreviousSibling,
    (gpointer)&ORBIT_domGetNextSibling,
    NULL, /* (gpointer)&ORBIT_domGetAttributes, */
    (gpointer)&ORBIT_domGetOwnerDocument,
    (gpointer)&ORBIT_domInsertBefore,
    (gpointer)&ORBIT_domReplaceChild,
    (gpointer)&ORBIT_domRemoveChild,
    (gpointer)&ORBIT_domAppendChild,
    (gpointer)&ORBIT_domHasChildNodes,
    (gpointer)&ORBIT_domCloneNode,
};

static POA_DOM_Node__vepv DOMNode_vepv = { &base_epv, &Node_epv };

/*
 * Static service lookup for Documents.
 */

static POA_DOM_Document__epv Document_epv = {
    NULL, /* _private */
    NULL, /* DOM_DocumentType(*_get_doctype) (PortableServer_Servant servant, CORBA_Environment * ev); */
    NULL, /* DOM_DOMImplementation(*_get_implementation) (PortableServer_Servant servant, CORBA_Environment * ev); */
    NULL, /* DOM_Element(*_get_documentElement) (PortableServer_Servant servant, CORBA_Environment * ev); */
    NULL, /* DOM_Element(*createElement) (PortableServer_Servant servant, DOMString * tagName, CORBA_Environment * ev); */
    NULL, /* DOM_DocumentFragment(*createDocumentFragment) (PortableServer_Servant servant, CORBA_Environment * ev); */
    NULL, /* DOM_Text(*createTextNode) (PortableServer_Servant servant, DOMString * data, CORBA_Environment * ev); */
    NULL, /* DOM_Comment(*createComment) (PortableServer_Servant servant, DOMString * data, CORBA_Environment * ev); */
    NULL, /* DOM_CDATASection(*createCDATASection) (PortableServer_Servant servant, DOMString * data, CORBA_Environment * ev); */
    NULL, /* DOM_ProcessingInstruction(*createProcessingInstruction) (PortableServer_Servant servant, DOMString * target, DOMString * data, CORBA_Environment * ev); */
    NULL, /* DOM_Attr(*createAttribute) (PortableServer_Servant servant, DOMString * name, CORBA_Environment * ev); */
    NULL, /* DOM_EntityReference(*createEntityReference) (PortableServer_Servant servant, DOMString * name, CORBA_Environment * ev); */
    NULL /* DOM_NodeList(*getElementsByTagName) (PortableServer_Servant servant, DOMString * tagname, CORBA_Environment * ev); */
};

static POA_DOM_Document__vepv DOMDocument_vepv =
         { &base_epv, &Node_epv, &Document_epv };

/************************************************************************
 *									*
 *			 Basic Servant Handing				*
 *									*
 ************************************************************************/

static DOM_Document DOM_DocumentServantRegister(domDocument doc,
                                                CORBA_Environment *ev) {
    CORBA_Object obj;
    char objid[50];
    static int objidNo = 0;
    PortableServer_ObjectId objidDOMImplementation = {0, 0, objid};
    int len;

    /*
     * Create a new servant.
     */
    doc->_private = NULL;
    doc->vepv = &DOMDocument_vepv;

    /*
     * initialize it.
     */
    POA_DOM_Document__init((POA_DOM_Document *) doc, ev);

    /*
     * Give it a new name and activate it.
     */
    len = g_snprintf(objid, 50, "DOM_Document_%d", ++objidNo);
    objidDOMImplementation._length = len + 1;
    PortableServer_POA_activate_object_with_id(poa, &objidDOMImplementation,
                                               doc, ev);

    /*
     * Get back the CORBA_Object and save it in the native object
     */
    obj = PortableServer_POA_servant_to_reference(poa, doc, ev);
    return((DOM_Document) obj);
}

static DOM_Node DOM_NodeServantRegister(domNode node, CORBA_Environment *ev) {
    DOM_NodeServantPiggy *servant;
    CORBA_Object obj;
    char objid[50];
    static int objidNo = 0;
    PortableServer_ObjectId objidDOMImplementation = {0, 0, objid};
    int len;

    /*
     * Create a new servant.
     */
    node->node._private = NULL;
    node->node.vepv = &DOMNode_vepv;

    /*
     * initialize it.
     */
    POA_DOM_Node__init((POA_DOM_Node *) node, ev);

    /*
     * Give it a new name and activate it.
     */
    len = g_snprintf(objid, 50, "DOM_Node_%d", ++objidNo);
    objidDOMImplementation._length = len + 1;
    PortableServer_POA_activate_object_with_id(poa, &objidDOMImplementation,
                                               node, ev);

    /*
     * Get back the CORBA_Object and save it in the native object
     */
    obj = PortableServer_POA_servant_to_reference(poa, node, ev);
    return((DOM_Node) obj);
}

/************************************************************************
 *									*
 *			 ORBit handling functions.			*
 *									*
 ************************************************************************/

int DOM_server(int *argc, char **argv) {
    PortableServer_ObjectId objidDOMImplementation =
             {0, sizeof("DOMImplementation"), "DOMImplementation"};
    PortableServer_ObjectId objidDOMServer =
             {0, sizeof("DOMServer"), "DOMServer"};

    CORBA_Environment ev;
    char *retval;
    CORBA_ORB orb;

    CORBA_exception_init(&ev);
    orb = CORBA_ORB_init(argc, argv, "orbit-local-orb", &ev);
    poa = (PortableServer_POA)
           CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
    PortableServer_POAManager_activate(
           PortableServer_POA__get_the_POAManager(poa, &ev), &ev);

    g_print("domclient ");


    POA_DOM_DOMImplementation__init(&DOMImplementation_servant, &ev);
    PortableServer_POA_activate_object_with_id(poa,
	   &objidDOMImplementation, &DOMImplementation_servant, &ev);
    DOMImplementation_client = PortableServer_POA_servant_to_reference(poa,
	   &DOMImplementation_servant, &ev);
    if (!DOMImplementation_client) {
	printf("Cannot get objref\n");
	return 1;
    }
    retval = CORBA_ORB_object_to_string(orb, DOMImplementation_client, &ev);
    g_print("%s ", retval); fflush(stdout);
    CORBA_free(retval);

    POA_DOM_Parser__init(&DOMParser_servant, &ev);
    PortableServer_POA_activate_object_with_id(poa,
	   &objidDOMServer, &DOMParser_servant, &ev);
    DOMParser_client = PortableServer_POA_servant_to_reference(poa,
	   &DOMParser_servant, &ev);
    if (!DOMParser_client) {
	printf("Cannot get objref\n");
	return 1;
    }
    retval = CORBA_ORB_object_to_string(orb, DOMParser_client, &ev);
    g_print("%s ", retval); fflush(stdout);
    CORBA_free(retval);

    g_print(" test.xml\n");

    CORBA_ORB_run(orb, &ev);

    return 0;
}
