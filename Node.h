/*
 * Node.h: interface for the Node module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_NODE_H__
#define __DOM_NODE_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"

/*
 * A DOM Node can be represented internally as either a pointer
 * to an xmlNode, an xmlDoc or an xmlAttr
 */

typedef union domNode {
    xmlNode	node;	/* This is an element, or a text leave */
    xmlAttr	attr;	/* An Attribute */
    xmlDoc	doc;	/* A document */
} domNodeStruct, *domNode;


#include "Document.h"

/*
 * Here are better notations for the type constants.
 */
#define DOM_ELEMENT_NODE		DOM_Node_ELEMENT_NODE
#define DOM_ATTRIBUTE_NODE		DOM_Node_ATTRIBUTE_NODE
#define DOM_TEXT_NODE			DOM_Node_TEXT_NODE
#define DOM_CDATA_SECTION_NODE		DOM_Node_CDATA_SECTION_NODE
#define DOM_ENTITY_REFERENCE_NODE	DOM_Node_ENTITY_REFERENCE_NODE
#define DOM_ENTITY_NODE			DOM_Node_ENTITY_NODE
#define DOM_PROCESSING_INSTRUCTION_NODE	DOM_Node_PROCESSING_INSTRUCTION_NODE
#define DOM_PI_NODE			DOM_Node_PROCESSING_INSTRUCTION_NODE
#define DOM_COMMENT_NODE		DOM_Node_COMMENT_NODE
#define DOM_DOCUMENT_NODE		DOM_Node_DOCUMENT_NODE
#define DOM_DOCUMENT_TYPE_NODE		DOM_Node_DOCUMENT_TYPE_NODE
#define DOM_DOCUMENT_FRAGMENT_NODE	DOM_Node_DOCUMENT_FRAGMENT_NODE
#define DOM_NOTATION_NODE		DOM_Node_NOTATION_NODE

extern domString domGetNodeName(domNode node, int *exc);
extern domString domGetNodeValue(domNode node, int *exc);
extern int       domSetNodeValue(domNode node, domString value, int *exc);
extern unsigned short domGetNodeType(domNode node, int *exc);
extern domNode   domGetParentNode(domNode node, int *exc);
extern domNode   domGetFirstChild(domNode node, int *exc);
extern domNode   domGetLastChild(domNode node, int *exc);
extern domNode   domGetPreviousSibling(domNode node, int *exc);
extern domNode   domGetNextSibling(domNode node, int *exc);
extern domDocument  domGetOwnerDocument(domNode node, int *exc);
extern domNode   domInsertBefore(domNode node, domNode newChild,
                                 domNode refChild, int *exc);
extern domNode   domReplaceChild(domNode node, domNode newChild,
                                 domNode oldChild, int *exc);
extern domNode   domRemoveChild(domNode node, domNode oldChild, int *exc);
extern domNode   domAppendChild(domNode node, domNode newChild, int *exc);
extern int       domHasChildNodes(domNode node, int *exc);
extern domNode   domCloneNode(domNode node, int deep, int *exc);
#endif /* __DOM_NODE_H__ */
