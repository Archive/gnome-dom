/*
 * CharacterData.h: interface for the CharacterData module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_CDATA_H__
#define __DOM_CDATA_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM CharacterData Node is represented internally as a
 * specific xmlNode.
 */

typedef domNodeStruct domCDATASectionStruct;
typedef domNode domCDATASection;

extern domString domGetData(domCDATASection data, int *exc);
extern void domSetData(domCDATASection data, domString value, int *exc);
extern unsigned long domGetLength(domCDATASection data, int *exc);
extern void domAppendData(domCDATASection data, domString arg, int *exc);
extern void domInsertData(domCDATASection data, unsigned long offset,
                          domString arg, int *exc);
extern void domDeleteData(domCDATASection data, unsigned long offset,
                          unsigned long count, int *exc);
extern void domReplaceData(domCDATASection data, unsigned long offset,
                          unsigned long count, domString arg, int *exc);

#endif /* __DOM_CDATA_H__ */
