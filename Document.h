/*
 * Document.h: interface for the Document module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_DOCUMENT_H__
#define __DOM_DOCUMENT_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"

/*
 * a DOM Document is handled by an xmlDoc structure.
 */

typedef xmlDocPtr domDocument;

#include "Node.h"
#include "Implementation.h"
#include "DocumentType.h"
#include "Element.h"
#include "EntityRef.h"
#include "DocumentFragment.h"
#include "Attr.h"
#include "Comment.h"
#include "Text.h"
#include "CData.h"
#include "PI.h"

extern domDocumentType domGetDocType(domDocument doc, int *exc);
extern domDOMImplementation domGetimplementation(domDocument doc, int *exc);
extern domElement domGetDocumentElement(domDocument doc, int *exc);
extern domElement domCreateElement(domDocument doc, domString tagName,
                                   int *exc);
extern domDocumentFragment domCreateDocumentFragment(domDocument doc, int *exc);
extern domText domCreateTextNode(domDocument doc, domString data, int *exc);
extern domComment domCreateComment(domDocument doc, domString data, int *exc);
extern domCDATASection domCreateCDATASection(domDocument doc, domString data,
                                             int *exc);
extern domProcessingInstruction domCreateProcessingInstruction(domDocument doc,
                              domString target, domString data, int *exc);
extern domAttr domCreateAttribute(domDocument doc, domString name, int *exc);
extern domEntityReference domCreateEntityReference(domDocument doc,
                                       domString name, int *exc);
extern domNodeList domGetElementsByTagName(domNode doc, domString tagname,
                                       int *exc);

#endif /* __DOM_DOCUMENT_H__ */
