/*
 * Comment.h: interface for the Comment module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_COMMENT_H__
#define __DOM_COMMENT_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM Comment is represented internally as an xmlNode.
 */

typedef domNodeStruct domCommentStruct;
typedef domNode domComment;

#endif /* __DOM_COMMENT_H__ */
