/*
 * Text.h: interface for the Text module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_TEXT_H__
#define __DOM_TEXT_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM Text is represented internally as an specific xmlText.
 */

typedef domNodeStruct domTextStruct;
typedef domNode domText;

extern domString domGetName(domText text, int *exc);
extern int domGetSpecified(domText text, int *exc);
extern domString domGetValue(domText text, int *exc);
extern void domSetValue(domText text, domString value, int *exc);

#endif /* __DOM_TEXT_H__ */
