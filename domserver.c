/*
 * domserver.c : A DOM server runtime. Takes call to load and parse
 *               XML docs, and then provides the DOM interface to walk
 *               and modify them.
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include "DOM.h"
#include "orbit-glue.h"

int main(int argc, char **argv) {
    int ret;

    ret = DOM_server(&argc, argv);
    exit(ret);
}

