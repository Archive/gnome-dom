/*
 * PI.h: interface for the ProcessingInstruction module for DOM.
 *
 * Specification: http://www.w3.org/TR/REC-DOM-Level-1/
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#ifndef __DOM_PROCESSING_INSTRUCTION_H__
#define __DOM_PROCESSING_INSTRUCTION_H__
#include <tree.h>
#include "DOM.h"
#include "DOMString.h"
#include "Node.h"

/*
 * A DOM ProcessingInstruction is represented internally as an xmlNode.
 */

typedef domNodeStruct domProcessingInstructionStruct;
typedef domNode domProcessingInstruction;

extern domString domGetTarget(domProcessingInstruction PI, int *exc);
extern domString domGetData(domProcessingInstruction PI, int *exc);
extern void domSetData(domProcessingInstruction PI, domString data, int *exc);

#endif /* __DOM_PROCESSING_INSTRUCTION_H__ */
