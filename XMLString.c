/*
 * XMLString.c : XMLString interfaces implementation.
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include "XMLString.h"

/*
 * xmlStrndup : a strndup for array of CHAR's
 */

CHAR *xmlStrndup(const CHAR *cur, int len) {
    CHAR *ret = malloc((len + 1) * sizeof(CHAR));

    if (ret == NULL) {
        fprintf(stderr, "malloc of %d byte failed\n",
	        (len + 1) * sizeof(CHAR));
        return(NULL);
    }
    memcpy(ret, cur, len * sizeof(CHAR));
    ret[len] = 0;
    return(ret);
}

/*
 * xmlStrdup : a strdup for CHAR's
 */

CHAR *xmlStrdup(const CHAR *cur) {
    const CHAR *p = cur;

    while (IS_CHAR(*p)) p++;
    return(xmlStrndup(cur, p - cur));
}

/*
 * xmlCharStrndup : a strndup for char's to CHAR's
 */

CHAR *xmlCharStrndup(const char *cur, int len) {
    int i;
    CHAR *ret = malloc((len + 1) * sizeof(CHAR));

    if (ret == NULL) {
        fprintf(stderr, "malloc of %d byte failed\n",
	        (len + 1) * sizeof(CHAR));
        return(NULL);
    }
    for (i = 0;i < len;i++)
        ret[i] = (CHAR) cur[i];
    ret[len] = 0;
    return(ret);
}

/*
 * xmlCharStrdup : a strdup for char's to CHAR's
 */

CHAR *xmlCharStrdup(const char *cur) {
    const char *p = cur;

    while (*p != '\0') p++;
    return(xmlCharStrndup(cur, p - cur));
}

/*
 * xmlStrcmp : a strcmp for CHAR's
 */

int xmlStrcmp(const CHAR *str1, const CHAR *str2) {
    register int tmp;

    do {
        tmp = *str1++ - *str2++;
	if (tmp != 0) return(tmp);
    } while ((*str1 != 0) && (*str2 != 0));
    return (*str1 - *str2);
}

/*
 * xmlStrncmp : a strncmp for CHAR's
 */

int xmlStrncmp(const CHAR *str1, const CHAR *str2, int len) {
    register int tmp;

    if (len <= 0) return(0);
    do {
        tmp = *str1++ - *str2++;
	if (tmp != 0) return(tmp);
	len--;
        if (len <= 0) return(0);
    } while ((*str1 != 0) && (*str2 != 0));
    return (*str1 - *str2);
}

/*
 * xmlStrchr : a strchr for CHAR's
 */

CHAR *xmlStrchr(const CHAR *str, CHAR val) {
    while (*str != 0) {
        if (*str == val) return((CHAR *) str);
	str++;
    }
    return(NULL);
}

/*
 * xmlStrlen : lenght of a CHAR's string
 */

int xmlStrlen(const CHAR *str) {
    int len = 0;

    if (str == NULL) return(0);
    while (*str != 0) {
	str++;
	len++;
    }
    return(len);
}

/*
 * xmlStrncat : a strncat for array of CHAR's
 */

CHAR *xmlStrncat(CHAR *cur, const CHAR *add, int len) {
    int size;
    CHAR *ret;

    if ((add == NULL) || (len == 0))
        return(cur);
    if (cur == NULL)
        return(xmlStrndup(add, len));

    size = xmlStrlen(cur);
    ret = realloc(cur, (size + len + 1) * sizeof(CHAR));
    if (ret == NULL) {
        fprintf(stderr, "xmlStrncat: realloc of %d byte failed\n",
	        (size + len + 1) * sizeof(CHAR));
        return(cur);
    }
    memcpy(&ret[size], add, len * sizeof(CHAR));
    ret[size + len] = 0;
    return(ret);
}

/*
 * xmlStrcat : a strcat for CHAR's
 */

CHAR *xmlStrcat(CHAR *cur, const CHAR *add) {
    const CHAR *p = add;

    if (add == NULL) return(cur);
    if (cur == NULL) 
        return(xmlStrdup(add));

    while (IS_CHAR(*p)) p++;
    return(xmlStrncat(cur, add, p - add));
}

