/*
 * Parser.c: Glue code for the XML Parser module for DOM.
 *
 * Specification: none, this is NOT covered by DOM Level 1 and subject
 *                to changes.
 *
 * Daniel Veillard <Daniel.Veillard@w3.org>
 */

#include <stdio.h>
#include "DOMString.h"
#include "Parser.h"
#include "parser.h"

domDocument domParseFile(domString filename, int *exc) {
    char *name;
    xmlDocPtr doc;

    name = domToString8(filename);
    if (name == NULL) return(NULL);
    
    doc = xmlParseFile(name);
    if (doc->name == NULL) doc->name = strdup(name);

fprintf(stderr, "xmlParseFile %s, Doc: %p\n", name, doc);

    free(name);
    return(doc);
}

domDocument domParseDoc (domString content, int *exc) {
    return(NULL);
}


